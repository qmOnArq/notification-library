package sk.monar.libNotif;

import android.content.Context;
import android.content.Intent;

public class SocketIOInit {
	public static void Start(String IP, Context ctx){
		Values.ServerAddress = IP;
		Intent in = new Intent();
		ctx.startService(in);
	}
	
	public static void End(Context ctx){
		Intent in = new Intent();
		ctx.stopService(in);
	}
}
