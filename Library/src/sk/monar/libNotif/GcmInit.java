package sk.monar.libNotif;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class GcmInit {

    public static interface RegisterCallback {
        public void onError(IOException e);
        public void onSuccess(String regId);
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String EXTRA_MESSAGE = "message";

    private static String SENDER_ID = "";
    private static String REG_ID = "";
    private static GoogleCloudMessaging gcm;
    private static RegisterCallback callback;

    public static String getSenderId(){
        return SENDER_ID;
    }
    public static String getRegId() {
        return REG_ID;
    }

    public static boolean init(Activity act, String senderId, RegisterCallback regCallback){
        callback = null;
        SENDER_ID = "";
        REG_ID = "";
        if(checkPlayServices(act)){
            callback = regCallback;
            SENDER_ID = senderId;
            REG_ID = getRegistrationId(act);
            if(REG_ID.equals("")){
                registerInBackground(act);
            } else {
                Debug.log("Device already registered, registration ID=" + REG_ID);
                callback.onSuccess(REG_ID);
            }
            return true;
        }
        return false;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private static boolean checkPlayServices(Activity act){
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(act);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, act, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Debug.log("This device is not supported.");
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private static String getRegistrationId(Activity act) {
        final SharedPreferences prefs = getGCMPreferences(act);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.equals("")) {
            Debug.log("Registration not found.");
            return "";
        }

        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(act);
        if (registeredVersion != currentVersion) {
            Debug.log("App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getGCMPreferences(Activity act) {
        return act.getSharedPreferences(act.getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Activity act) {
        try {
            PackageInfo packageInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param act application's activity.
     * @param regId registration ID
     */
    private static void storeRegistrationId(Activity act, String regId) {
        final SharedPreferences prefs = getGCMPreferences(act);
        int appVersion = getAppVersion(act);
        Debug.log("Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private static void registerInBackground(final Activity act) {
        new AsyncTask<Void, String, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(act);
                    }
                    String regid = gcm.register(getSenderId());
                    Debug.log("Device registered, registration ID=" + regid);
                    msg = regid;

                    storeRegistrationId(act, regid);
                } catch (IOException ex) {
                    Debug.log("Error: " + ex.getMessage());
                    msg = "";
                    callback.onError(ex);
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                storeRegistrationId(act, msg);
                REG_ID = msg;
                if(!msg.equals(""))
                    callback.onSuccess(msg);
            }
        }.execute();
    }
}
