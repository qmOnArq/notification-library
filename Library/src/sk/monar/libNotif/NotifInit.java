package sk.monar.libNotif;

import java.util.HashMap;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

public class NotifInit {
	private static Class<? extends Activity> startupActivity;
	private static final int NOTIFICATION_ID = 1;
	
	public static Class<? extends Activity> getStartupActivity(){
		return startupActivity;
	}
	
	public static void setStartupActivity(Class<? extends Activity> activityClass){
		startupActivity = activityClass;
	}
	
	private static HashMap<String, Integer> smallIcons = new HashMap<String, Integer>();
	
	public static void addSmallIcon(String name, Integer id){
		smallIcons.put(name, id);
	}
	
	public static Integer getSmallIcon(String name){
		if(!smallIcons.containsKey(name)) return null;
		return smallIcons.get(name);
	}
	
	private static HashMap<String, Bitmap> bigIcons = new HashMap<String, Bitmap>();
	
	public static void addBigIcon(String name, Bitmap bmp){
		bigIcons.put(name, bmp);
	}
	
	public static Bitmap getBigIcon(String name){
		if(!bigIcons.containsKey(name)) return null;
		return bigIcons.get(name);
	}
	
	protected static void sendNotification(Bundle extras, Context ctx) {
    	NotificationManager mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

    	Intent i = new Intent(ctx, NotifInit.getStartupActivity());
    	i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, i, 0);
        
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx).setAutoCancel(true);
        mBuilder.setSmallIcon(android.R.drawable.btn_dropdown);       
        if(extras.containsKey("title")) mBuilder.setContentTitle(extras.getString("title"));
        if(extras.containsKey("content")) mBuilder.setContentText(extras.getString("content"));
        if(extras.containsKey("cInfo")) mBuilder.setContentInfo(extras.getString("cInfo"));
        if(extras.containsKey("time")) mBuilder.setWhen(extras.getLong("time"));
        if(extras.containsKey("sIcon"))      
        	if(NotifInit.getSmallIcon(extras.getString("sIcon")) != null)
        		mBuilder.setSmallIcon(NotifInit.getSmallIcon(extras.getString("sIcon")));
        if(extras.containsKey("lIcon"))      
        	if(NotifInit.getBigIcon(extras.getString("lIcon")) != null)
        		mBuilder.setLargeIcon(NotifInit.getBigIcon(extras.getString("lIcon")));      	
        
        if(extras.containsKey("useBig") && extras.getString("useBig").equals("true")){ 
        	NotificationCompat.BigTextStyle bs = new NotificationCompat.BigTextStyle();
        	mBuilder.setStyle(bs);
        	if(extras.containsKey("content")) bs.bigText(extras.getString("content"));    
        	if(extras.containsKey("bTitle")) bs.setBigContentTitle(extras.getString("bTitle"));
        	if(extras.containsKey("bText")) bs.bigText(extras.getString("bText"));
        	if(extras.containsKey("bSummary")) bs.setSummaryText(extras.getString("bSummary"));
        }
                        

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
