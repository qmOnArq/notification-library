package sk.monar.libNotif;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by mOnAr on 30.11.2013.
 */
public abstract class GcmIntentService extends IntentService {
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Debug.log("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Debug.log("Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Debug.log("Received: " + extras.toString());
            }
            boolean heartbeat = false;
			if(extras.containsKey("$$ignore$$")){
            	heartbeat = extras.getString("$$ignore$$").equals("true");
            }
            handleMessage(extras, messageType, heartbeat);
        }
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    public abstract void handleMessage(Bundle msg, String type, boolean heartbeat);
}
