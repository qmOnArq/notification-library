package sk.monar.libNotif;

import android.util.Log;

/**
 * Created by mOnAr on 30.11.2013.
 */
public class Debug {
    private static boolean LogAllowed = true;

    public static void setLogging(boolean value){
        LogAllowed = value;
    }

    public static void log(String message){
        if(LogAllowed){
            Log.i(Values.TAG, message);
        }
    }
}
