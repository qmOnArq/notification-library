package sk.monar.application;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;
import sk.monar.libNotif.GcmIntentService;

/**
 * Created by mOnAr on 3.12.2013.
 */
public class ExampleGcmIntentService extends GcmIntentService {
	
	private Handler handler;
	
	@Override
	public void onCreate() {
	    super.onCreate();
	    handler = new Handler();
	}

    @Override
    public void handleMessage(final Bundle msg, final String type, final boolean heartbeat){
    	handler.post(new Runnable(){
			@Override
			public void run() {
				Toast.makeText(ExampleGcmIntentService.this, heartbeat ? "Heartbeat" : type + ": " + msg.toString(), Toast.LENGTH_SHORT).show();				
			}    		
    	});
    	
    }
}
