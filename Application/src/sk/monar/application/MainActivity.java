package sk.monar.application;

import java.io.IOException;

import sk.monar.libNotif.GcmInit;
import sk.monar.libNotif.NotifInit;
import sk.monar.libNotif.SocketIOInit;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private class RegisterCallback implements GcmInit.RegisterCallback {	
		@Override
		public void onSuccess(final String regId) {	
			MainActivity.this.runOnUiThread(new Runnable() {									
				@Override
				public void run() {
					((TextView)findViewById(R.id.tvGcmId)).setText(regId);	
					((Button)findViewById(R.id.bSendId)).setEnabled(true);
				}
			});
		}
		
		@Override
		public void onError(final IOException e) {
			MainActivity.this.runOnUiThread(new Runnable() {									
				@Override
				public void run() {
					((TextView)findViewById(R.id.tvGcmId)).setText("Error");	
					((Button)findViewById(R.id.bSendId)).setEnabled(false);
				}
			});
		}
	}
	
	
	private void ShowToast(final CharSequence text){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
			}
		});
	}
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		NotifInit.addSmallIcon("small", R.drawable.ic_launcher);
		NotifInit.addBigIcon("big", BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher));
		NotifInit.setStartupActivity(MainActivity.class);
		
		((TextView)findViewById(R.id.tvGcmId)).setText(GcmInit.getRegId());	
		((TextView)findViewById(R.id.tvInstallId)).setText(ExampleID.id(this)); 
		((Button)findViewById(R.id.bRegisterGCM)).setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {		
				GcmInit.init(MainActivity.this, "529179425427", new RegisterCallback());
			}
		});
		
		((Button)findViewById(R.id.bSendId)).setOnClickListener(new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				new HTTPGetTask(new HTTPGetTask.HTTPGetCallback() {					
					@Override
					public void onResult(final String result) {
						ShowToast("ID Sent: " + result);
					}
					
					@Override
					public void onError(final Exception e) {
						ShowToast("Error: " + e.getMessage());
					}
				}).execute("http://95.85.23.171:3000/notif/users", "add", "true", "id", ExampleID.id(MainActivity.this), "key", GcmInit.getRegId());
			}
		});
		
		((Button)findViewById(R.id.bConnect)).setOnClickListener( new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				SocketIOInit.Start("95.85.23.171", MainActivity.this);				
			}
		});
		
		((Button)findViewById(R.id.bDisconnect)).setOnClickListener( new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				SocketIOInit.End(MainActivity.this);				
			}
		});
	}

}
