package sk.monar.application;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

public class HTTPGetTask extends AsyncTask<String, Void, String> {
	
	public static interface HTTPGetCallback{
		public void onResult(String result);
		public void onError(Exception e);
	}
	
	private HTTPGetCallback cb;
	
	public HTTPGetTask(HTTPGetCallback callback){
		cb = callback;
	}

	@Override
	protected String doInBackground(String... params) {
		HttpClient httpClient = new DefaultHttpClient();  
		String url = params[0];
		for(int i = 1; i < params.length; i += 2){
			url += (i == 1) ? "?" : "&";
			url += params[i] + "=" + params[i + 1];
		}
		HttpGet httpGet = new HttpGet(url);
		try {
		    HttpResponse response = httpClient.execute(httpGet);
		    StatusLine statusLine = response.getStatusLine();
		    if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
		        HttpEntity entity = response.getEntity();
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		        entity.writeTo(out);
		        out.close();
		        return out.toString();
		    } else {
		    	if(cb != null){
					cb.onError(new IOException("Wrong response"));
				}
		    }
		} catch (ClientProtocolException e) {
			if(cb != null){
				cb.onError(e);
			}
		} catch (IOException e) {
			if(cb != null){
				cb.onError(e);
			}
		}		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if(cb != null && result != null){
			cb.onResult(result);
		}
    }

}
