
var API_KEY = "";
var EnableLogging = false;
var https = require('https');
var db = null;
var heartbeat = false;
var heartbeatIntervalId = null;
var heartbeatInterval = 3 * 60 * 1000;
var debug_io = null;
var io = null;

exports.SetLogging = function(enable){
    EnableLogging = enable;
};

exports.SetAPIKey = function(key){
    API_KEY = key;
};

exports.SetDebugSocketIO = function(socketIO){
    debug_io = socketIO;
};

var heartbeatFunction = function(){
    exports.ListGCMKeys(function(keys, err){
        if(keys && keys.length > 0){
            keys.forEach(function(key){
                exports.SendGCM({"$$ignore$$": true}, [key["key"]], null, null, function(success, info){
                    if(EnableLogging){
                        debug_io.sockets.emit('debug', { msg: "GCM Heartbeat: " + info });
                    }
                });
            });
        }
    });
};

exports.SetHeartbeat = function(enable){
    heartbeat = enable;
    if(heartbeatIntervalId != null){
        clearInterval(heartbeatIntervalId);
    }

    if(heartbeat){
        heartbeatFunction();
        heartbeatIntervalId = setInterval(heartbeatFunction, heartbeatInterval);
    }
};

exports.GetHeartbeat = function(){
    return heartbeat;
};

exports.SetDB = function(newDB){
    var collections = ["GCMKeys"];
    db = require("mongojs").connect(newDB, collections);
};

exports.SetGCMKey = function(_id, _key, callback){
    if(db === null){
        throw new Error("Database not set");
    }

    db.GCMKeys.find({id: _id}, function(err, keys) {
        if(keys && keys.length > 0){
            db.GCMKeys.update({id: _id}, {$set: {key: _key}}, function(err, updated) {
                callback("Updated", err);
            });
        } else {
            db.GCMKeys.save({id: _id, key: _key}, function(err, saved) {
                callback(saved, err);
            });
        }
    });
};

exports.ListGCMKeys = function(callback){
    if(db === null){
        throw new Error("Database not set");
    }

    db.GCMKeys.find({}, function(err, keys) {
        callback(keys, err);
    });
};

var SendGCM = function(data, callback){
    if(API_KEY === ""){
        throw new Error("API Key not set");
    }

    var post_options = {
        hostname: 'android.googleapis.com',
        port: '443',
        path: '/gcm/send',
        method: 'POST',
        headers: {
            'Authorization': "key=" + API_KEY,
            'Content-Type': 'application/json'
        }
    };

    var post_req = https.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            if(EnableLogging)
                console.log('Response: ' + chunk);
            if(callback)
                callback(true, chunk);
        });
    });

    post_req.on('error', function (err) {
        if(EnableLogging)
            console.log('Error: ' + err);
        if(callback)
            callback(false, err);
    });

    post_req.write(JSON.stringify(data));
    post_req.end();
};

exports.SendGCM = function(message, devices, TTL, delay, callback){
    var data = {};
    data.data = message;
    data.registration_ids = devices;
    data.time_to_live = TTL;
    data.delay_while_idle = delay;
    SendGCM(data, callback);
};

exports.SendNot = function(title, lIcon, content, cInfo, sIcon, time, useBig, bTitle, bSummary, bText, useGcm, devices, callback) {
    var data = {};
    data.title = title;
    data.lIcon = lIcon;
    data.content = content;
    data.cInfo = cInfo;
    data.sIcon = sIcon;
    data.time = time;
    if(time != null){
        var tmpDate = new Date(time.split(' ').join('T'));
        data.time = tmpDate.getTime();
    }
    data.useBig = useBig;
    data.bTitle = bTitle;
    data.bSummary = bSummary;
    data.bText = bText;
    data['$$notif$$'] = true;

    if(useGcm){
        var obj = {};
        obj.data = data;
        exports.ListGCMKeys(function(keys, err){
            obj.registration_ids = [];
            if(keys && devices){
                for(var i = 0; i < devices.length; i++){
                    for(var j = 0; j < keys.length; j++){
                        if(keys[j].id === devices[i]){
                            obj.registration_ids.push(keys[j].key);
                            break;
                        }
                    }
                }
            }
            SendGCM(obj, callback);
        });
    } else {

    }
};

exports.StartSocketIOServer = function() {
    io = require('socket.io');
    io.set("heartbeat interval","180");
    io.set("heartbeat timeout","360");
    io.listen(3001);
}