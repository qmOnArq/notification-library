
var structure = {
    title: 'Notification Library',

    menu: [
        {name: 'Simple GCM', url: './#1'},
        {name: 'GCM+', url: './#2'},
        {name: 'Standalone', url: './#3'},
        {name: 'Send Notification', url: './#4'},
        {name: 'Database', url: './#5'}
    ]
};

/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var notif = require('./lib');

var app = express();
var io = null;

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(express.favicon(path.join(__dirname, 'public/images/favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(function (req, res, next) {
    req.structure = res.structure = structure;
    req.notif = res.notif = notif;
    req.io = res.io = io;
    next();
});
app.use(app.router);

notif.SetLogging(true);
notif.SetAPIKey("AIzaSyBqnBLOzEVSyVpPV6LRFPu_i41tWPGg9mM");
notif.SetDB("notif");

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/notif/', routes.index);
app.get('/notif/users', user.list);

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

io = require('socket.io').listen(server);

notif.SetDebugSocketIO(io);
notif.StartSocketIOServer();

io.sockets.on('connection', function (socket) {
    socket.emit('debug', { msg: 'Connected to console.' });
    socket.on('gcm-send', function(data){
        var message = null;
        if(data.MESSAGEedit != null){
            try {
                message = JSON.parse(data.MESSAGEedit);
            } catch(e) {
                socket.emit('debug', { msg: "Error Message: " + e });
            }
        }

        var devices = null;
        if(data.IDSedit != null){
            try {
                var ids = data.IDSedit.split(" ").join("").split('"').join("").split("'").join("");
                ids = ids.split(",");
                for (var i = 0; i < ids.length; ++i) {
                    ids[i] = '"' + ids[i] + '"';
                }
                ids = ids.join(", ");
                devices = JSON.parse("[" + ids + "]");
            } catch(e) {
                socket.emit('debug', { msg: "Error IDs: " + e });
            }
        }

        var TTL = null;
        if(data.TTLedit != null){
            TTL = parseInt(data.TTLedit);
            if(isNaN(TTL)){
                socket.emit('debug', { msg: "Error TTL: is NaN." });
                TTL = null;
            }
        }

        var delay = data.delay != null;

        notif.SendGCM(message, devices, TTL, delay, function(success, message){
            socket.emit('debug', { msg: "GCM Response: " + message });
        });
    });

    socket.on('gcm-plus', function(data){
        notif.SetHeartbeat(data.heartbeat != null);
        socket.emit('debug', { msg: "Value set." });
    });

    socket.on('not-send', function(data){
        var title = data.notTitle;
        var lIcon = data.notLargeIcon;
        var content = data.notContent;
        var cInfo = data.notContentInfo;
        var sIcon = data.notSmallIcon;
        var time = data.notTime;
        var useBig = data.notUseBig != null;
        var bTitle = data.notBigTitle;
        var bSummary = data.notBigSummary;
        var bText = data.notBigText;
        var useGcm = data.notDelOpt === "gcm";
        var devices = null;
        if(data.notIDs != null){
            try {
                var ids = data.notIDs.split(" ").join("").split('"').join("").split("'").join("");
                ids = ids.split(",");
                for (var i = 0; i < ids.length; ++i) {
                    ids[i] = '"' + ids[i] + '"';
                }
                ids = ids.join(", ");
                devices = JSON.parse("[" + ids + "]");
            } catch(e) {
                socket.emit('debug', { msg: "Error IDs: " + e });
            }
        }
        notif.SendNot(title, lIcon, content, cInfo, sIcon, time, useBig, bTitle, bSummary, bText, useGcm, devices, function(success, message){
            socket.emit('debug', { msg: "GCM Response: " + message });
        });
    });
});