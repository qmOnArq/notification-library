
/*
 * GET users listing.
 */

exports.list = function(req, res){
    if(req.query.add === "true"){
        req.notif.SetGCMKey(req.query.id, req.query.key, function(added, err){
            res.send(added);
        });
    } else {
        req.notif.ListGCMKeys(function(keys, err){
            res.send(keys);
        });
    }
};