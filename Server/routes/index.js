
/*
 * GET home page.
 */

exports.index = function(req, res){
    req.structure['page'] = 0;
    req.structure['info'] = null;
    req.structure['heartbeat'] = req.notif.GetHeartbeat();
    res.render('layout', req.structure);
};

exports.standalone = function(req, res){
    req.structure['page'] = 2;
    req.structure['info'] = null;
    res.render('standalone', req.structure);
};